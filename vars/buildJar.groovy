#!/usr/bin/env groovy

def call() {
    echo "building the application for the pipeline $BRANCH_NAME"
    sh 'mvn package'
}
